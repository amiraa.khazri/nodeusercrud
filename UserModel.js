const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const userSchema = new Schema({
    nom : String,
    prenom: String,
    age: String,
    email: String
}, {
    timestamps: true /* date d'action */
})

module.exports= mongoose.model('users',userSchema)