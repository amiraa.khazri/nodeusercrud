const express=require('express');
const router = express.Router();
const controller = require('./controller');

/* http request */

router.post('/users',controller.addUser)
router.get('/users',controller.getUsers)
router.get('/users/:id',controller.getUser)
router.patch('/users/:id',controller.updateUser)
router.delete('/users/:id',controller.deleteUser)

/* ----- on peut simplifier le code comme suit en deux lignes ----- */
/*
router.route('/users').get(controller.getUsers).post(controller.addUser)
router.route('/users/:id').get(controller.getUser).patch(controller.updateUser).delete(controller.deleteUser)
*/

module.exports= router