const express = require('express');
const app = express();
require('dotenv').config();
const mongoose = require('mongoose');
const router = require('./router');
var bodyParser = require('body-parser')

/*Body-Parser*/
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())



/* Router */
    app.use('/api', router)

/* connect database*/
mongoose.connect(process.env.DB_URI)
    .then( ()=> console.log('connected...'))
    .catch( err => console.log(err))

/*  run server */
const port = process.env.PORT
app.listen(port, () => console.log(`server run in port :: ${port}`))